BUILD_FOLDER="build.ios64"

echo "Removing Cmake cache"
rm -rf $BUILD_FOLDER
rm -rf CMakeCache.txt
rm -rf CMakeFiles
rm -rf CMakeScripts
rm -rf cmake_install.cmake

echo "Generating new Xcode project"
cmake -G Xcode \
-DCMAKE_PREFIX_PATH:STRING=$HOME/Qt/5.10.1/ios \
-DCMAKE_CXX_COMPILER:STRING=/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/clang++ \
-DCMAKE_C_COMPILER:STRING=/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/clang \
-DCMAKE_TOOLCHAIN_FILE=./ios.toolchain.cmake \
-DIOS_PLATFORM=OS64 \
-DIOS_ARCH="arm64" \
-DIOS_DEPLOYMENT_TARGET=10.0 \
-H. \
-B$BUILD_FOLDER
