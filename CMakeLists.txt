cmake_minimum_required (VERSION 3.7)

set(DEVELOPMENT_PROJECT_NAME "project")                     # <== Set to your project name, e.g. project.xcodeproj
set(DEVELOPMENT_TEAM_ID "HKB93E894N")                       # <== Set to your team ID from Apple
set(APP_NAME "YOURAPP")                                     # <== Set To your app's name
set(APP_BUNDLE_IDENTIFIER "com.company.app")                # <== Set to your app's bundle identifier
set(CODE_SIGN_IDENTITY "iPhone Developer")                  # <== Set to your preferred code sign identity, to see list:
                                                            # /usr/bin/env xcrun security find-identity -v -p codesigning
set(DEPLOYMENT_TARGET 10.0)                                  # <== Set your deployment target version of iOS
set(DEVICE_FAMILY "1,2")                                      # <== Set to "1" to target iPhone, set to "2" to target iPad, set to "1,2" to target both

project(${DEVELOPMENT_PROJECT_NAME})

set(PRODUCT_NAME ${APP_NAME})
set(EXECUTABLE_NAME ${APP_NAME})
set(MACOSX_BUNDLE_EXECUTABLE_NAME ${APP_NAME})
set(MACOSX_BUNDLE_INFO_STRING ${APP_BUNDLE_IDENTIFIER})
set(MACOSX_BUNDLE_GUI_IDENTIFIER ${APP_BUNDLE_IDENTIFIER})
set(MACOSX_BUNDLE_BUNDLE_NAME ${APP_BUNDLE_IDENTIFIER})
set(MACOSX_BUNDLE_ICON_FILE "")
set(MACOSX_BUNDLE_LONG_VERSION_STRING "1.0")
set(MACOSX_BUNDLE_SHORT_VERSION_STRING "1.0")
set(MACOSX_BUNDLE_BUNDLE_VERSION "1.0")
set(MACOSX_BUNDLE_COPYRIGHT "Copyright YOU")
set(MACOSX_DEPLOYMENT_TARGET ${DEPLOYMENT_TARGET})

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON) #necessary for Qt QML support : the necessary qrc_qml.cpp file is being generated if this is enabled
set(CMAKE_AUTORCC ON) #necessary for Qt QML support : the necessary qrc_qml.cpp file is being generated if this is enabled
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(APP_SOURCE_FILES
  ./main.mm
  ./src/QtImportPlugin.h #necessary for Qt QML support :
#if this is missing or not `#include`ed in main.cpp, we get runtime errors like
# "QQmlApplicationEngine failed to load component
# qrc:/main.qml:1 module "QtQuick" is not installed"
)

set(RESOURCES
  ./LaunchScreen.storyboard
)

set(QRC_FILES
  ./src/qml/qml.qrc
)

find_package(Qt5 COMPONENTS Core Gui OpenGL Quick Svg Xml Positioning Location Multimedia Network Concurrent QuickControls2 REQUIRED)
qt5_add_resources(QT_GENERATED_SOURCES_FROM_QRC_FILES ${QRC_FILES}) #necessary for Qt QML support : this one requests the path for the generated qrc_qml.cpp file

add_executable(
    ${APP_NAME}
    MACOSX_BUNDLE
    ${APP_SOURCE_FILES}
    ${QT_GENERATED_SOURCES_FROM_QRC_FILES}
    ${RESOURCES}
)

# add qml files into the XCode project to be able to easily edit them (can be opened in the same project - don't need to open separate Xcode window or other editor)
file(GLOB_RECURSE QML_FILES RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}/src/qml" "*.qml" "*.js" "*.qrc")
foreach(FILE_PATH ${QML_FILES})
    get_filename_component(FILE_NAME ${FILE_PATH} NAME)
    get_filename_component(GROUP_NAME ${FILE_PATH} DIRECTORY)

    target_sources(${APP_NAME} PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/src/qml/${FILE_PATH}")

    source_group(QML\\${GROUP_NAME} FILES "${CMAKE_CURRENT_SOURCE_DIR}/src/qml/${FILE_PATH}")
endforeach()
source_group(QRC FILES ${QRC_FILES})

# Locate system libraries on iOS
find_library(UIKIT UIKit)
find_library(ASSETSLIBRARY AssetsLibrary)
find_library(OPENGLES OpenGLES)
find_library(FOUNDATION Foundation)
find_library(QUARTZCORE QuartzCore)
find_library(COREFOUNDATION CoreFoundation)
find_library(CORETEXT CoreText)
find_library(COREGRAPHICS CoreGraphics)
find_library(MOBILECORESERVICES MobileCoreServices)
find_library(CFNETWORK CFNetwork)
find_library(SYSTEMCONFIGURATION SystemConfiguration)
find_library(SECURITY Security)
find_library(IMAGEIO ImageIO)
find_library(COREMEDIA CoreMedia)
find_library(AVFOUNDATION AVFoundation)
find_library(COREVIDEO CoreVideo)
find_library(CORELOCATION CoreLocation)
find_library(AUDIOTOOLBOX AudioToolbox)

# link the frameworks located above (necessary for Qt support - Qt libraries are using these, but Qt is not linking these by default)
target_link_libraries(${APP_NAME} PRIVATE ${UIKIT})
target_link_libraries(${APP_NAME} PRIVATE ${ASSETSLIBRARY})
target_link_libraries(${APP_NAME} PRIVATE ${OPENGLES})
target_link_libraries(${APP_NAME} PRIVATE ${FOUNDATION})
target_link_libraries(${APP_NAME} PRIVATE ${QUARTZCORE})
target_link_libraries(${APP_NAME} PRIVATE ${COREFOUNDATION})
target_link_libraries(${APP_NAME} PRIVATE ${CORETEXT})
target_link_libraries(${APP_NAME} PRIVATE ${COREGRAPHICS})
target_link_libraries(${APP_NAME} PRIVATE ${MOBILECORESERVICES})
target_link_libraries(${APP_NAME} PRIVATE ${CFNETWORK})
target_link_libraries(${APP_NAME} PRIVATE ${SYSTEMCONFIGURATION})
target_link_libraries(${APP_NAME} PRIVATE ${SECURITY})
target_link_libraries(${APP_NAME} PRIVATE ${IMAGEIO})
target_link_libraries(${APP_NAME} PRIVATE ${COREMEDIA})
target_link_libraries(${APP_NAME} PRIVATE ${AVFOUNDATION})
target_link_libraries(${APP_NAME} PRIVATE ${COREVIDEO})
target_link_libraries(${APP_NAME} PRIVATE ${CORELOCATION})
target_link_libraries(${APP_NAME} PRIVATE ${AUDIOTOOLBOX})
target_link_libraries(${APP_NAME} PRIVATE
    Qt5::Core
    Qt5::Gui
    Qt5::Quick
    Qt5::QuickControls2
    Qt5::Positioning
    Qt5::Svg
    Qt5::Xml
    Qt5::Location
    Qt5::Network
    Qt5::Concurrent
    Qt5::OpenGL
    Qt5::Multimedia
    Qt5::Qml
    Qt5::QIOSIntegrationPlugin #necessary for Qt support : without this we receive linker errors
)

#necessary for Qt support : without this we receive linker errors
set(CMAKE_EXE_LINKER_FLAGS
  "${CMAKE_EXE_LINKER_FLAGS}
  -force_load  ${CMAKE_PREFIX_PATH}/plugins/platforms/libqios.a
  -lz"
)

set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,-e,_qt_main_wrapper") #necessary for Qt support :
#without this we receive the error described here :
# https://stackoverflow.com/questions/45508043/qt-ios-linker-error-entry-point-main-undefined/45617820#45617820
# https://forum.qt.io/topic/82102/can-t-create-qapplication-for-ios-using-cmake-build

set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -u _qt_registerPlatformPlugin") #necessary for Qt QML support :
#without this we receive the error described here :
# https://stackoverflow.com/questions/45508043/qt-ios-linker-error-entry-point-main-undefined/45617820#45617820

#search and link a library
function(find_and_link_library)
foreach(lib ${ARGV})
    find_library(_${lib}_t_ ${lib} PATH_SUFFIXES "qml/QtQuick/Controls.2"
        "qml/QtQuick/Controls"
        "qml/QtQuick/Templates.2"
        "qml/QtLocation"
        "qml/QtQuick/Window.2"
        "qml/QtPositioning"
        "qml/QtQuick.2"
        "qml/QtQuick/Controls.2/Material"
        "qml/QtQuick/Layouts"
        "qml/QtQuick/Extras"
        "qml/QtGraphicalEffects"
        "qml/QtGraphicalEffects/private"
        "qml/QtMultimedia"
        "qml/QtQuick/Shapes"
        "qml/QtQuick/Dialogs"
        "qml/QtQuick/Dialogs/Private"
        "qml/QtQuick/PrivateWidgets"
        "qml/Qt/labs/settings"
        "qml/Qt/labs/folderlistmodel"
        "plugins/iconengines"
        "plugins/imageformats"
        "plugins/mediaservice"
        "plugins/geoservices"
        "plugins/position"
        "plugins/platforms/darwin"
    REQUIRED)

    target_link_libraries(${APP_NAME} PRIVATE ${_${lib}_t_})
    target_sources(${APP_NAME} PRIVATE ${_${lib}_t_})
    source_group(Libraries FILES ${_${lib}_t_})
endforeach(lib)
endfunction(find_and_link_library)

#Qt libraries
#Based on :
# $ pwd
# $HOME/Qt/5.10.1/ios
# $ find ./ -name *.a
#and common sense
#and based on : 
#an Xcode project that is generated by qmake from a Qt Creator generated project that is using qmake ( using the command : $HOME/Qt/<version>/ios/bin/qmake *.pro -spec macx-xcode )
find_and_link_library(qtharfbuzz
    qtpcre2
    qtlibpng
    clip2tri
    clipper
    poly2tri
    qtquick2plugin
    Qt5ClipboardSupport
    Qt5GraphicsSupport
    Qt5FontDatabaseSupport
    qtquickcontrols2plugin
    qtquickcontrolsplugin
    qtquicktemplates2plugin
    Qt5QuickTemplates2
    Qt5QuickControls2
    windowplugin
    declarative_location
    declarative_positioning
    qquicklayoutsplugin
    qtquickcontrols2materialstyleplugin
    qtquickextrasplugin
    qtgraphicaleffectsplugin
    qtgraphicaleffectsprivate
    qmlshapesplugin
    dialogplugin
    dialogsprivateplugin
    widgetsplugin
    qmlsettingsplugin
    qmlfolderlistmodelplugin
    qsvgicon
    qsvg
    qjpeg
    qtposition_cl
    qtposition_positionpoll
    qtgeoservices_itemsoverlay
    qtiff
    qtga
    Qt5Multimedia
    Qt5MultimediaQuick
    declarative_multimedia
    qavfcamera
    qavfmediaplayer
    qiosnsphotolibrarysupport
)

# Turn on ARC
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fobjc-arc")

# Create the app target
set_target_properties(${APP_NAME} PROPERTIES
                      XCODE_ATTRIBUTE_DEBUG_INFORMATION_FORMAT "dwarf-with-dsym"
                      RESOURCE "${RESOURCES}"
                      XCODE_ATTRIBUTE_GCC_PRECOMPILE_PREFIX_HEADER "YES"
                      XCODE_ATTRIBUTE_IPHONEOS_DEPLOYMENT_TARGET ${DEPLOYMENT_TARGET}
                      XCODE_ATTRIBUTE_CODE_SIGN_IDENTITY ${CODE_SIGN_IDENTITY}
                      XCODE_ATTRIBUTE_DEVELOPMENT_TEAM ${DEVELOPMENT_TEAM_ID}
                      XCODE_ATTRIBUTE_TARGETED_DEVICE_FAMILY ${DEVICE_FAMILY}
                      MACOSX_BUNDLE_INFO_PLIST ${CMAKE_CURRENT_SOURCE_DIR}/plist.in
                      XCODE_ATTRIBUTE_CLANG_ENABLE_OBJC_ARC YES
                      XCODE_ATTRIBUTE_COMBINE_HIDPI_IMAGES NO
                      XCODE_ATTRIBUTE_INSTALL_PATH "$(LOCAL_APPS_DIR)"
                      XCODE_ATTRIBUTE_ENABLE_TESTABILITY YES
                      XCODE_ATTRIBUTE_GCC_SYMBOLS_PRIVATE_EXTERN YES
                      XCODE_ATTRIBUTE_ENABLE_BITCODE "No"
)

# Set the app's linker search path to the default location on iOS
set_target_properties(
    ${APP_NAME}
    PROPERTIES
    XCODE_ATTRIBUTE_LD_RUNPATH_SEARCH_PATHS
    "@executable_path/Frameworks"
)
