# Qt iOS Template

A template project with a basic "Hello World" app that is using CMake + linking Qt + targeting iOS. 

## How it is made?

The basics of it is created by following these steps :

  1. CMake doesn't have first class iOS support yet. ( The implementation of it is currently in progress : https://gitlab.kitware.com/cmake/cmake/issues/17870 . ) So we copied the ios-cmake project from here : https://github.com/sheldonth/ios-cmake . Removed unncessary parts from it's `CMakeLists.txt` : the parts relevant to it's sample C++ framework and the automated tests and all the commented out parts. 
  2. Created a project using Qt Creator that is using `qmake` build system, minimum Qt version for it is `5.10` , and iOS simulator and iOS device kits are configured for it.
  3. Modified `main.qml` to be Qt Quick compatible : had to replace `title: qsTr("Hello World")` with 
  ```
  Text
  {
    font.bold: true
    text: "Hello World!"
    font.pixelSize: 20
  } 
  ```
  4. Generated an Xcode project from this `*.pro` project by executing this command : `$HOME/Qt/<version>/ios/bin/qmake *.pro -spec macx-xcode`
  5. Based on the Xcode project that is generated in step #3 : Updated `CMakeLists.txt` (we should already have it after step #1) to link the same libraries and to use the same compiler and linker flags and header search paths.
  6. Added `QtImportPlugin.h` that contains the same `Q_IMPORT_PLUGIN(...)` Qt plugin imports as the files that are generated when executing step #3 : `<project_name>_qml_plugin_import.cpp` and `<project_name>_plugin_import.cpp`.

## Building and running the app

- Install Qt (if not already installed)
  - You must install Qt SDK version 5.10.1
- Add this to your `$HOME/.profile` file : `export Qt5_DIR=$HOME/Qt/5.10.1/`
- Install CMake (if not already installed)
- Install Xcode (if not already installed)
- Install Command Line Tools (if not already installed) ( by launching Terminal.app and executing `xcode-select --install` )
- Open `CMakeLists.txt`
  - Set lines 3-10 with values for your project
- Create Xcode project for Devices (arm64) and Simulators (x86_64)
  - Run `generate-projects.sh` to create Xcode projects
  - Run `open ./build.ios64/*.xcodeproj` or `open ./build.iossimulator64/*.xcodeproj`
  - In the Xcode window that is opened up as the result of the previous command : select your application in the target selector (in the top left corner), select the target ( a device or a simulator ), an press the run button
  
## iPhone/iPad support
- To build an iPhone/iPad target, change the value of `DEVICE_FAMILY` on line 14 of `CMakeLists.txt` to `"1,2"`
  
## Notes
- Currently opening the project in Qt Creator is not supported. Reason : CMake support had lower priority for Qt than qmake support so far. Looks they'll change to CMake as the build system for Qt in Qt 6, so we hope things improve a bit there. We tried to add Qt Creator support : we could build open the project and build the app using custom commands, but currently there is no way to deploy and run it on iOS simulator or device. Only qmake based projects have this ability.
- We had to choose between two options : 
  1. having support for a wider functionality ( and have all the fixes necessary to make those available )
  2. having support for just the basic functionality
   In case if we choose #2, this template's users would have to deal with adding support for more functionalities ( so they would have to find all the necessary fixes for making it available as well )
   In case if you would like to make your app slimmer, feel free to try to remove lines with `target_link_libraries` and `find_and_link_library` and entries from `src/QtImportPlugin.h` combined until your app still compiles.

## References
- We had to apply many workarounds to make this work. You can find more details it under the following links :
  - https://stackoverflow.com/questions/25042916/run-time-error-for-qt-application-on-ios-built-via-cmake
  - https://stackoverflow.com/questions/45508043/qt-ios-linker-error-entry-point-main-undefined/45617820#45617820
  - https://forum.qt.io/topic/82102/can-t-create-qapplication-for-ios-using-cmake-build
  - https://bugreports.qt.io/browse/QTBUG-38913
  - https://bugreports.qt.io/browse/QTBUG-47336 (the priority is "Not important" 🙄)

## Thanks To

* [ios-cmake-template-app](https://github.com/sheldonth/ios-cmake) - this one is a simple iOS application template (without Qt).
* [ios-cmake-toolchain](https://github.com/leetal/ios-cmake) - ios toolchain support for CMake
